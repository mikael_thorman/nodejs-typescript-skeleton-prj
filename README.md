# nodejs-typescript-skeleton-prj

A skeleton "hello World"-project for a NodeJS project using Typescript.

More info can be found in [this file](./setup.md)

#### To start the development environment:

```shell
npm run dev
```

#### To run the server in production mode:

```shell
npm run prod
```
