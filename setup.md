# Create & setup project

# Tutorial used: 
This example was made with inspiration from this site:

https://medium.com/javascript-in-plain-english/typescript-with-node-and-express-js-why-when-and-how-eb6bc73edd5d

## Create project

```shell
npm init
```

```shell
npm install typescript --save
```

Inside our package.json we will put a script called tsc:

```json
"scripts": {
    "tsc": "tsc"
},
```

This modification allows us to call typescript functions from the command line in the project’s folder. So we can use the following command:

```shell
npm run tsc -- --init
```

Uncomment outDir in tsconfig.json, set to 
"outDir": "./build",

## Installing express.js

```shell
npm install express --save

npm install @types/express --save-dev
```

Created ./src/app.ts

# Run & Compile app

## Run app
build:
```shell
npm run tsc
```
Run:

```shell
node build/app.js
```

## Running TypeScript without transpiling
You can run typescript directly on the node with the ts-node package.

This package is recommended for development only. To make the final deploy in production, always use the javascript version of your project.

The ts-node is already included as a dependency on another package, tts-node-dev. After installing,ts-node-dev we can run commands that restarts the server whenever a project file changes.

```shell
npm install ts-node-dev -s
```

Inside our packege.json we will add two more scripts:

```json
"scripts": {
    "tsc": "tsc",
    "dev": "ts-node-dev --respawn --transpileOnly ./app/app.ts",
    "prod": "tsc && node ./build/app.js"
},
```


### To start the development environment:

```shell
npm run dev
```

### To run the server in production mode:

```shell
npm run prod
```
